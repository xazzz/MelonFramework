<?php
/**
 * Melon － 可用于php5.3或以上的开源框架
 * 
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @link http://git.oschina.net/397574898/MelonFramework
 * @author Melon <denglh1990@qq.com>
 * @version 0.1.0
 */

namespace Melon\Exception;

defined('IN_MELON') or die('Permission denied');

/*
 * 异常处理
 * 
 * @package Melon
 * @since 0.1.0
 * @author Melon
 */
class RuntimeException extends \Exception {
	
}